#ifndef POSE_HPP
#define POSE_HPP
#pragma once

#include <iostream>

#include <opencv2/opencv.hpp>

namespace salih_slam {

    class Pose {
    /** @brief Pose - class storing and managing translation and orientation for salih slam system **/
        private:
            cv::Mat translation_ ; 

            cv::Mat orientation_ ;

        public:
            /** @brief Pose (constructor) - deep copies given matrices after validating them
              * @param const cv::Mat& - const lvalue reference to cv::Mat relating to translation
              * @param const cv::Mat& - const lvalue reference to cv::Mat relating to orientation **/
            Pose(const cv::Mat&, const cv::Mat&) noexcept(false) ;

            /** @brief Pose (constructor) - copy constructor deep copies properties of existing Pose object
              * @param const Pose& - const lvalue reference to Pose object to (deep) copy values of **/
            Pose(const Pose&) noexcept ;

            /** @brief Assignment operator - copy assignment operator deep copies matrices after validating them
              * @param const Pose& - const lvalue reference to Pose object to (deep) copy values of
              * @return Pose& - lvalue reference to created Pose object **/
            Pose& operator=(const Pose&) noexcept ;

            /** @brief Pose (constructor) - move constructor takes reference to properties of existing Pose object
              * @param Pose&& - rvalue reference to Pose object to shallow copy values of **/
            Pose(Pose&&) noexcept ;

            /** @brief Assignment operator - move assignment operator takes reference to properties of existing Pose object
              * @param Pose&& - rvalue reference to Pose object to shallow copy values of
              * @param Pose& - lvalue reference to Pose object created **/
            Pose& operator=(Pose&&) noexcept ;

            /** translation (overload) - returns const reference to translation Mat object
             * @return const cv::Mat& - const lvalue reference to cv::Mat object containing new translation value **/
            const cv::Mat& translation() const noexcept ;

            /** translation (overload) - verifies and sets new translation matrix values
              * @param const cv::Mat& - const lvalue reference to cv::Mat containing translation matrix **/
            void translation(const cv::Mat&) noexcept(false) ;

            /** translation (overload) - inline verifies and sets new translation matrix values
              * @param const cv::Mat& - const rvalue reference to cv::Mat containing translation matrix **/
            void translation(const cv::Mat&&) noexcept(false) ;

            /** orientation (overload) - returns const reference to orientation Mat object
             * @return const cv::Mat& - const lvalue reference to cv::Mat object containing new orientation value **/
            const cv::Mat& orientation() const noexcept ;

            /** orientation (overload) - verifies and sets new orientation matrix values
              * @param const cv::Mat& - const lvalue reference to cv::Mat containing orientation matrix **/
            void orientation(const cv::Mat&) noexcept(false) ;

            /** orientation (overload) - verifies and sets new orientation matrix values
              * @param const cv::Mat& - const rvalue reference to cv::Mat containing orientation matrix **/
            void orientation(const cv::Mat&&) noexcept(false) ;

            /** ~Pose (destructor) - destructor triggers memory release held by internal cv::Mat's **/
            ~Pose() noexcept ;

            friend ::std::ostream& operator<<(::std::ostream&, const Pose&) noexcept ; 

            // DELETED Pose - empty constructor (makes no sense for this program, as it must be given in config)
            Pose() = delete ;
    } ;

    /** @brief output stream operator - std::ostream output function
      * @param ::std::ostream& - lvalue reference to output stream object
      * @param const Pose& - const lvalue reference to Pose object
      * @return ::std::ostream& - lvalue reference to output stream object **/
    ::std::ostream& operator<<(::std::ostream&, const Pose&) noexcept ;

} ;

#endif // POSE_HPP
