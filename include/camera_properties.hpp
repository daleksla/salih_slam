#ifndef CAMERA_INFO_HPP
#define CAMERA_INFO_HPP
#pragma once

#include <iostream>

#include <opencv2/opencv.hpp>

namespace salih_slam {

    struct CameraProperties {
        /** @brief CameraProperties - basic struct to store camera information. Won't make it a class on principle' **/
        private: // no setter overloads for these properties as these are immutable in reality
            cv::Mat camera_matrix_ ;

            cv::Mat distortion_coefficients_ ;

            cv::Mat projection_matrix_ ;

        public:
            /** @brief CameraProperties (constructor) - deep copies given matrices after validating them
              * @param const cv::Mat& - const lvalue reference to cv::Mat relating to camera matrix
              * @param const cv::Mat& - const lvalue reference to cv::Mat relating to distortion coefficients matrix
              * @param const cv::Mat& - const lvalue reference to cv::Mat relating to projection matrix **/
            CameraProperties(const cv::Mat&, const cv::Mat&, const cv::Mat&) noexcept(false) ;

            /** @brief CameraProperties (constructor) - copy constructor deep copies properties of existing CameraInfo object
              * @param const CameraInfo& - const lvalue reference to CameraInfo object to (deep) copy values of **/
            CameraProperties(const CameraProperties&) noexcept ;

            /** @brief Assignment operator - copy assignment operator deep copies matrices after validating them
              * @param const CameraProperties& - const lvalue reference to CameraInfo object to (deep) copy values of
              * @return CameraProperties& - lvalue reference to created CameraInfo object **/
            CameraProperties& operator=(const CameraProperties&) noexcept ;

            /** @brief CameraProperties (constructor) - move constructor takes reference to properties of existing CameraInfo object
              * @param CameraProperties&& - rvalue reference to CameraInfo object to shallow copy values of **/
            CameraProperties(CameraProperties&&) noexcept ;

            /** @brief Assignment operator - move assignment operator takes reference to properties of existing CameraInfo object
              * @param CameraProperties&& - rvalue reference to CameraInfo object to shallow copy values of
              * @param CameraProperties& - lvalue reference to CameraInfo object created **/
            CameraProperties& operator=(CameraProperties&&) noexcept ;

            /** camera_matrix - returns const reference to Mat object
             * @return const cv::Mat& - const lvalue reference to cv::Mat object containing camera matrix values **/
            const cv::Mat& camera_matrix() const noexcept ;

            /** distortion_coefficients - returns const reference to Mat object
             * @return const cv::Mat& - const lvalue reference to cv::Mat object containing distortion coefficients values **/
            const cv::Mat& distortion_coefficients() const noexcept ;

            /** projection_matrix - returns const reference to Mat object
             * @return const cv::Mat& - const lvalue reference to cv::Mat object containing projection values **/
            const cv::Mat& projection_matrix() const noexcept ;

            /** ~CameraProperties (destructor) - destructor triggers memory release held by internal cv::Mat's **/
            ~CameraProperties() noexcept ;

            friend ::std::ostream& operator<<(::std::ostream&, const CameraProperties&) noexcept ; 

            // DELETED CameraProperties - empty constructor (makes no sense for this program, as it must be given in config)
            CameraProperties() = delete ;
    } ;
    
    /** @brief output stream operator - std::ostream output function
      * @param ::std::ostream& - lvalue reference to output stream object
      * @param const CameraProperties& - const lvalue reference to CameraInfo object
      * @return ::std::ostream& - lvalue reference to output stream object **/
    ::std::ostream& operator<<(::std::ostream&, const CameraProperties&) noexcept ;

} ;

#endif // CAMERA_INFO_HPP
