#ifndef PREPROCESSING_HPP
#define PREPROCESSING_HPP
#pragma once

#include <string>
#include <filesystem>
#include <tuple>

#include <opencv2/opencv.hpp>

#include "camera_properties.hpp"
#include "pose.hpp"

/** @brief File contains the declarations related to preprocessing necessary data for salih slam system **/

namespace salih_slam {

    /** @brief is_meta_directory (overload) - determines whether given name of path is to a directory
      * @param const std::string& - const lvalue reference to string storing path name to test
      * @return bool - whether given path leads to directory **/ 
    bool is_meta_directory(const std::string&) noexcept ; 

    /** @brief is_meta_directory (overload) - determines whether given name of path is to a directory
      * @param const std::filesystem::path& - const lvalue reference to filesystem path object initialised to path
      * @return bool - whether given path leads to directory **/ 
    bool is_meta_directory(const std::filesystem::path&) noexcept ; 

    /** @brief is_meta_yaml (overload) - determines whether given name of path is to a YAML file
      * @param const std::string& - const lvalue reference to string storing path name to test
      * @return bool - whether given path leads to file with YAML file **/
    bool is_meta_yaml(const std::string&) noexcept ;

    /** @brief is_meta_yaml (overload) - determines whether given name of path is to an image
      * @param const std::filesystem::path& - const lvalue reference to filesystem object initialised to path
      * @return bool - whether given path leads to file with YAML file **/
    bool is_meta_yaml(const std::filesystem::path&) noexcept ;

    /** @brief is_meta_image (overload) - determines whether given name of path is to an image
      * @param const std::string& - const lvalue reference to string storing path name to test
      * @return bool - whether given path leads to image **/
    bool is_meta_image(const std::string&) noexcept ;

    /** @brief is_meta_image (overload) - determines whether given name of path is to an image
      * @param const std::filesystem::path& - const lvalue reference to filesystem object initialised to path
      * @return bool - whether given path leads to image **/
    bool is_meta_image(const std::filesystem::path&) noexcept ;

    /** @brief is_image_extension - tests whether file extension mathces a known image extension
      * @param const std::string& - const lvalue reference to string containing a supposed image's extension
      * @return bool - whether a given extension is a known image extension **/
    bool is_image_extension(const std::string&) noexcept ;

    /** @brief get_image_names (overload) - extracts all image names from a given directory
      * @param const std::string& - const lvalue reference to string storing path name to take images from
      * @return std::vector<std::string> - list of paths to image **/
    std::vector<std::string> get_image_names(const std::string&) noexcept ;

    /** @brief get_image_names (overload) - extracts all image names from a given directory
      * @param const std::filesystem::path& - const lvalue reference to filesystem object initialised to path
      * @return std::vector<std::string> - list of paths to image **/
    std::vector<std::string> get_image_names(const std::filesystem::path&) noexcept ;

    /** @brief extract_camera_info - reads specified YAML calibration file for camera imfo used in one-time operation, checks validity
      * @param const std::string& - const lvalue reference to string storing camera-info-storing file name
      * @return std::tuple<salih_slam::CameraProperties, salih_slam::Pose> - tuple storing camera properties - intrinsic camera matrix, distortion coefficients, projection matrix - and Pose - initial translation, initial orientation **/
    std::tuple<salih_slam::CameraProperties, salih_slam::Pose> extract_camera_info(const std::string&) noexcept(false) ;

}

#endif // PREPROCESSING_HPP
