#ifndef FEATURES_HPP
#define FEATURES_HPP
#pragma once

#include <tuple>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>

/** @brief File storing declarations relating to feature extraction for images **/

namespace salih_slam {

    /** @brief extract_features - returns detected features of a given image
      * @param cv::Ptr<cv::ORB>& - reference to shared pointer (opencv's typedef) to feature (ORB) detector
      * @param const std::string& - const reference to string path to image
      * @return std::tuple<std::vector<cv::KeyPoint>, cv::Mat> - tuple collection of a list of feature positions & descriptions **/
    std::tuple<std::vector<cv::KeyPoint>, cv::Mat> extract_features(cv::Ptr<cv::ORB>&, const std::string&) ;

    /** @brief extract_features - returns detected features of a given image
      * @param cv::Ptr<cv::ORB>& - reference to shared pointer (opencv's typedef) to feature (ORB) detector
      * @param const cv::Mat& - const reference to opencv image
      * @return std::tuple<std::vector<cv::KeyPoint>, cv::Mat> - tuple collection of a list of feature positions & descriptions **/
    std::tuple<std::vector<cv::KeyPoint>, cv::Mat> extract_features(cv::Ptr<cv::ORB>&, const cv::Mat&) ;

    /** @brief match_features - determine features matched between two given Mat frames
      * @param const cv::Mat& - const lvalue reference to Mat style feature descriptors
      * @param const cv::Mat& - const lvalue reference to Mat style feature descriptors
      * @return std::vector<cv::DMatch> - list of matched feature points **/
    std::vector<cv::DMatch> match_features(const cv::Mat& , const cv::Mat&) ;

    /** @brief ratio_test_matches - filter matches using Lowe's ratio test
      * @param const std::vector<cv::DMatch>& - const lvalue reference to vector of match points
      * @return std::vector<cv::DMatch> - list of matched feature points **/
    std::vector<cv::DMatch> ratio_test_matches(const std::vector<cv::DMatch>& matches) ;

    /** @brief symmetrise_features - determine features shared between two given Mat frames
      * @param const std::vector<cv::DMatch>& - const lvalue reference to one set of matched feature points
      * @param const std::vector<cv::DMatch>& - const lvalue reference to another set of matched feature points
      * @return std::vector<cv::DMatch> - list of commonly matched feature points **/
    std::vector<cv::DMatch> symmetrise_features(const std::vector<cv::DMatch>& , const std::vector<cv::DMatch>&) ;

}

#endif // FEATURES_HPP
