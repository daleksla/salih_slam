# salih_slam
## README

Basic Visual Simultaneous Localisation & Mapping (VSLAM) implementation for three dimensional localisation.

***

Written in C++, powered by the [opencv](https://www.opencv.org/) library.

See LICENSE for terms of usage.
