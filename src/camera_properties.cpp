#include <iostream>
#include <stdexcept>
#include <string>

#include <opencv2/opencv.hpp>

#include "camera_properties.hpp"

salih_slam::CameraProperties::CameraProperties(const cv::Mat& camera_matrix, const cv::Mat& distortion_coefficients, const cv::Mat& projection_matrix) noexcept(false)
{
    if(camera_matrix.cols != 3 || camera_matrix.rows != 3)
    {
        const std::string msg = std::string("Camera matrix requires dimension of 3*3, but Matrix provided has dimensions of ") + std::to_string(camera_matrix.rows) + std::string("*") + std::to_string(camera_matrix.cols) ;
        throw std::invalid_argument(msg) ;
    }
    this->camera_matrix_ = camera_matrix.clone() ;

    if(distortion_coefficients.cols != 5 || distortion_coefficients.rows != 1)
    {
        const std::string msg = std::string("Distortion coefficients matrix requires dimension of 1*5, but Matrix provided has dimensions of ") + std::to_string(distortion_coefficients.rows) + std::string("*") + std::to_string(distortion_coefficients.cols) ;
        throw std::invalid_argument(msg) ;
    }
    this->distortion_coefficients_ = distortion_coefficients.clone() ;
    
    if(projection_matrix.cols != 4 || projection_matrix.rows != 3)
    {
        const std::string msg = std::string("Projection matrix requires dimension of 3*4, but Matrix provided has dimensions of ") + std::to_string(projection_matrix.rows) + std::string("*") + std::to_string(projection_matrix.cols) ;
        throw std::invalid_argument(msg) ;
    }
    this->projection_matrix_ = projection_matrix.clone() ;
}

salih_slam::CameraProperties::CameraProperties(const salih_slam::CameraProperties& pose_obj) noexcept
{
    this->camera_matrix_ = pose_obj.camera_matrix_.clone() ;
    this->distortion_coefficients_ = pose_obj.distortion_coefficients_.clone() ;
    this->projection_matrix_ = pose_obj.projection_matrix_.clone() ;
}

salih_slam::CameraProperties& salih_slam::CameraProperties::operator=(const salih_slam::CameraProperties& CameraInfoObj) noexcept
{
    this->camera_matrix_ = CameraInfoObj.camera_matrix_.clone() ;
    this->distortion_coefficients_ = CameraInfoObj.distortion_coefficients_.clone() ;
    this->projection_matrix_ = CameraInfoObj.projection_matrix_.clone() ;
    return *this ;
}

salih_slam::CameraProperties::CameraProperties(salih_slam::CameraProperties&& CameraInfoObj) noexcept
{
    this->camera_matrix_ = CameraInfoObj.camera_matrix_ ;
    this->distortion_coefficients_ = CameraInfoObj.distortion_coefficients_ ;
    this->projection_matrix_ = CameraInfoObj.projection_matrix_ ;
    CameraInfoObj.camera_matrix_ = cv::Mat() ;
    CameraInfoObj.distortion_coefficients_ = cv::Mat() ;
    CameraInfoObj.projection_matrix_ = cv::Mat() ;
}

salih_slam::CameraProperties& salih_slam::CameraProperties::operator=(salih_slam::CameraProperties&& CameraInfoObj) noexcept
{
    this->camera_matrix_ = CameraInfoObj.camera_matrix_ ;
    this->distortion_coefficients_ = CameraInfoObj.distortion_coefficients_ ;
    this->projection_matrix_ = CameraInfoObj.projection_matrix_ ;
    CameraInfoObj.camera_matrix_ = cv::Mat() ;
    CameraInfoObj.distortion_coefficients_ = cv::Mat() ;
    CameraInfoObj.projection_matrix_ = cv::Mat() ;
    return *this ;
}

const cv::Mat& salih_slam::CameraProperties::camera_matrix() const noexcept
{
    return this->camera_matrix_ ;
}

const cv::Mat& salih_slam::CameraProperties::distortion_coefficients() const noexcept
{
    return this->distortion_coefficients_ ;
}

const cv::Mat& salih_slam::CameraProperties::projection_matrix() const noexcept
{
    return this->projection_matrix_ ;
}

salih_slam::CameraProperties::~CameraProperties() noexcept {} ;

::std::ostream& salih_slam::operator<<(::std::ostream& stream, const salih_slam::CameraProperties& obj) noexcept
{
    stream << "Camera parameters: " << '\n' ;
    stream << "\tCamera Matrix: " << obj.camera_matrix_ << '\n' ;
    stream << "\tDistortion Coefficients: " << obj.distortion_coefficients_ << '\n' ;
    stream << "\tProjection Matrix: " << obj.projection_matrix_ << '\n' ;
    return stream ;
}
