#include <filesystem>
#include <string>
#include <vector>
#include <iostream>
#include <cstddef>
#include <stdexcept>
#include <tuple>

#include <opencv2/opencv.hpp>

#include "yaml-cpp/yaml.h"

#include "preprocessing.hpp"
#include "camera_properties.hpp"
#include "pose.hpp"

/** @brief File contains the definitions related to preprocessing necessary data for salih slam system **/

bool salih_slam::is_meta_directory(const std::string& dir_name) noexcept
{
    std::filesystem::path dir_object(dir_name) ;
    return salih_slam::is_meta_directory(dir_object) ;
}

bool salih_slam::is_meta_directory(const std::filesystem::path& dir_object) noexcept
{
    if(!std::filesystem::is_directory(dir_object))
    {
        return false ;
    }
    else if(std::filesystem::is_symlink(dir_object))
    {
        std::error_code error_code ;
        std::filesystem::path re_dir_object = std::filesystem::read_symlink(dir_object, error_code) ; 
        if(!error_code)
        {
            return false ;
        }
        
        if(!std::filesystem::is_directory(re_dir_object))
        {
            return false ;
        }
    }
    
    if(!std::filesystem::is_directory(dir_object))
    {
        return false ;
    }
    else if(std::filesystem::is_symlink(dir_object))
    {
        std::error_code error_code ;
        const std::filesystem::path re_dir_object = std::filesystem::read_symlink(dir_object, error_code) ; 
        if(!error_code || !std::filesystem::is_directory(re_dir_object))
        {
            return false ;
        }
    }

    return true ;
}

bool salih_slam::is_meta_yaml(const std::string& file_name) noexcept
{
    std::filesystem::path file_object(file_name) ;
    return salih_slam::is_meta_yaml(file_object) ;
}

bool salih_slam::is_meta_yaml(const std::filesystem::path& file_obj) noexcept
{
    if(!std::filesystem::is_regular_file(file_obj))
    {
        return false ;
    }
    else if(std::filesystem::is_symlink(file_obj))
    {
        std::error_code error_code ;
        const std::filesystem::path re_file_object = std::filesystem::read_symlink(file_obj, error_code) ; 
        if(!error_code || std::filesystem::is_directory(re_file_object) || re_file_object.extension().string() == ".yml" || re_file_object.extension().string() == ".yaml")
        {
            return false ;
        }
    }
    
    return true ;
}

bool salih_slam::is_meta_image(const std::string& img_name) noexcept
{
    const std::filesystem::path img_object(img_name) ;
    return salih_slam::is_meta_image(img_object) ;
}

bool salih_slam::is_meta_image(const std::filesystem::path& img_object) noexcept
{
    if(!std::filesystem::is_regular_file(img_object) || !salih_slam::is_image_extension(img_object.extension().string()))
    {
        return false ;
    }
    else if(std::filesystem::is_symlink(img_object))
    {
        std::error_code error_code ;
        const std::filesystem::path re_img_object = std::filesystem::read_symlink(img_object, error_code) ; 
        if(!error_code || std::filesystem::is_directory(re_img_object) || !salih_slam::is_image_extension(re_img_object.extension().string()))
        {
            return false ;
        }
    }

    return true ;
}

bool salih_slam::is_image_extension(const std::string& file_extension) noexcept
{
    const std::vector<std::string> image_extensions = {
        ".png",
        ".jpg", ".jpeg", ".jpe", ".jif", ".jfif", ".jfi",
        ".tiff", ".tif",
        ".jp2", ".j2k", ".jpf", ".jpx", ".jpm", ".mj2",
        ".svg", ".svgz"
    } ;

    for(const auto& i : image_extensions)
    {
        if(file_extension == i)
        {
            return true ;
        }
    }

    return false ;
}

std::vector<std::string> salih_slam::get_image_names(const std::string& image_dir_name) noexcept
{
    return salih_slam::get_image_names(std::filesystem::path(image_dir_name)) ;
}

std::vector<std::string> salih_slam::get_image_names(const std::filesystem::path& image_dir_object) noexcept
{
    std::vector<std::string> image_names ;
    for(const auto& entry : std::filesystem::directory_iterator(image_dir_object))
    {
        const std::string file_name(entry.path().string()) ;
        if(salih_slam::is_meta_image(file_name))
        {
            image_names.push_back(file_name) ;
        }
    }
    return image_names ;
}

std::tuple<salih_slam::CameraProperties, salih_slam::Pose> salih_slam::extract_camera_info(const std::string& config_file) noexcept(false)
{
    if(!salih_slam::is_meta_yaml(config_file))
    {
        const std::string msg = std::string("Camera information file '") + config_file + std::string("' is not a valid YAML file") ;
        throw std::invalid_argument(msg) ;
    }

    const YAML::Node config = YAML::LoadFile(config_file) ;

    auto extract_matrix = [&config](const std::string& matrix_name) -> cv::Mat { // internal lambda to extract yaml matrix
        YAML::Node matrix_cont = config[matrix_name] ;

        const auto col_num = matrix_cont["cols"].as<std::size_t>() ;

        const auto row_num = matrix_cont["rows"].as<std::size_t>() ;

        const auto mat_data = matrix_cont["data"] ; 
        std::vector<double> values ; for(const auto& val : mat_data) values.push_back(val.as<double>()) ;
        
        const cv::Mat config_mat = cv::Mat(row_num, col_num, CV_64F, {values.data()}).clone() ; // we clone because this constructor makes a shallow reference to the vector data, whilst clone forces the data to be copied over into a new mat

        return config_mat ;
    } ;

    const cv::Mat camera_matrix = extract_matrix("camera_matrix") ;
    const cv::Mat distortion_coefficients = extract_matrix("distortion_coefficients") ;
    const cv::Mat projection_matrix = extract_matrix("projection_matrix") ;
    const cv::Mat translation_matrix = extract_matrix("translation") ;
    const cv::Mat orientation_matrix = extract_matrix("orientation") ;

    return std::tuple<salih_slam::CameraProperties, salih_slam::Pose>{
        salih_slam::CameraProperties{camera_matrix, distortion_coefficients, projection_matrix},
        salih_slam::Pose{translation_matrix, orientation_matrix}
    } ;
}
