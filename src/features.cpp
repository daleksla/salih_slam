#include <cstddef>
#include <vector>
#include <memory>
#include <algorithm>

#include <opencv2/opencv.hpp>

#include "features.hpp"

/** @brief File storing definitions relating to feature extraction for images **/

std::tuple<std::vector<cv::KeyPoint>, cv::Mat> salih_slam::extract_features(cv::Ptr<cv::ORB>& feature_detector, const std::string& input_name)
{
    return salih_slam::extract_features(feature_detector, cv::imread(input_name)) ;
}

std::tuple<std::vector<cv::KeyPoint>, cv::Mat> salih_slam::extract_features(cv::Ptr<cv::ORB>& feature_detector, const cv::Mat& input_img)
{
    //cv::Ptr<cv::ORB> orb_obj = cv::ORB::create() ; // note: ptr is a typedef to std::shared_ptr
    
    std::vector<cv::KeyPoint> key_points ; // vector to store detected feature points
    feature_detector->detect(input_img, key_points) ;

    cv::Mat descriptors ; // vector to store detected descriptors
    feature_detector->compute(input_img, key_points, descriptors) ;

    return std::tuple<std::vector<cv::KeyPoint>, cv::Mat>{key_points, descriptors}  ;
}

std::vector<cv::DMatch> salih_slam::match_features(const cv::Mat& feature_descriptors_1 , const cv::Mat& feature_descriptors_2)
{
    cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_HAMMING, true) ;

    std::vector<cv::DMatch> matched_features ; 
    matcher->match(feature_descriptors_1, feature_descriptors_2, matched_features) ;
    
    return matched_features ;
}

std::vector<cv::DMatch> salih_slam::ratio_test_matches(const std::vector<cv::DMatch>& matches)
{
    std::vector<cv::DMatch> ratiod_matches ;
    const float ratio_thresh = 0.75f;
    for(auto it = std::cbegin(matches) ; it != std::cend(matches) ; it = std::next(it))
    {
        if(it->distance < ratio_thresh * (it+1)->distance)
        {
            ratiod_matches.push_back(*it);
        }
    }
    return ratiod_matches ;
}

std::vector<cv::DMatch> salih_slam::symmetrise_features(const std::vector<cv::DMatch>& matched_features_1, const std::vector<cv::DMatch>& matched_features_2)
{
    std::vector<cv::DMatch> symmetric_features ;

    auto feature_match = [](const cv::DMatch& a, const cv::DMatch& b) { 
        return (a.queryIdx == b.trainIdx) && (a.trainIdx == b.queryIdx) ;
    } ;

    for(const auto& matched_feature_1 : matched_features_1)
    {
        for(const auto& matched_feature_2 : matched_features_2)
        {
            if(feature_match(matched_feature_1, matched_feature_2))
            {
                symmetric_features.push_back(matched_feature_1) ;
            }
        }
    }

    return symmetric_features ;
}
