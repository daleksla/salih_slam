#include <iostream>
#include <stdexcept>
#include <string>

#include <opencv2/opencv.hpp>

#include "pose.hpp"

salih_slam::Pose::Pose(const cv::Mat& translation, const cv::Mat& orientation) noexcept(false)
{
    this->translation(translation) ;
    this->orientation(orientation) ;
}

salih_slam::Pose::Pose(const salih_slam::Pose& pose_obj) noexcept
{
    this->translation_ = pose_obj.translation_.clone() ;
    this->orientation_ = pose_obj.orientation_.clone() ;
}

salih_slam::Pose& salih_slam::Pose::operator=(const salih_slam::Pose& pose_obj) noexcept
{
    this->translation_ = pose_obj.translation_.clone() ;
    this->orientation_ = pose_obj.orientation_.clone() ;
    return *this ;
}

salih_slam::Pose::Pose(salih_slam::Pose&& pose_obj) noexcept
{
    this->translation_ = pose_obj.translation_ ;
    this->orientation_ = pose_obj.orientation_ ;
    pose_obj.translation_ = cv::Mat() ;
    pose_obj.orientation_ = cv::Mat() ;
}

salih_slam::Pose& salih_slam::Pose::operator=(salih_slam::Pose&& pose_obj) noexcept
{
    this->translation_ = pose_obj.translation_ ;
    this->orientation_ = pose_obj.orientation_ ;
    return *this ;
}

const cv::Mat& salih_slam::Pose::translation() const noexcept
{
    return this->translation_ ;
}

void salih_slam::Pose::translation(const cv::Mat& translation) noexcept(false)
{
    if(translation.cols != 1 || translation.rows != 3)
    {
        const std::string msg = std::string("Translation matrix requires dimension of 3*1, but Matrix provided has dimensions of ") + std::to_string(translation.rows) + std::string("*") + std::to_string(translation.cols) ;
        throw std::invalid_argument(msg) ;
    }
    this->translation_ = translation.clone() ;
}

void salih_slam::Pose::translation(const cv::Mat&& translation) noexcept(false)
{
    this->translation(translation) ;
}

const cv::Mat& salih_slam::Pose::orientation() const noexcept
{
    return this->orientation_ ;
}

void salih_slam::Pose::orientation(const cv::Mat& orientation) noexcept(false)
{
    if(orientation.cols != 1 || orientation.rows != 4)
    {
        const std::string msg = std::string("Orientation matrix requires dimension of 4*1, but Matrix provided has dimensions of ") + std::to_string(orientation.rows) + std::string("*") + std::to_string(orientation.cols) ;
        throw std::invalid_argument(msg) ;
    }

    this->orientation_ = orientation.clone() ;
}

void salih_slam::Pose::orientation(const cv::Mat&& orientation) noexcept(false)
{
   this->orientation(orientation) ;
}

salih_slam::Pose::~Pose() noexcept {} ;

::std::ostream& salih_slam::operator<<(::std::ostream& stream, const salih_slam::Pose& obj) noexcept
{
    stream << "Pose: " << '\n' ;
    stream << "\tTranslation: " << obj.translation_ << '\n' ;
    stream << "\tOrientation: " << obj.orientation_ << '\n' ;
    return stream ;
}
