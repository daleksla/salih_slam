#include <iostream>
#include <string>
#include <vector>
#include <cstddef>
#include <algorithm>
#include <cstring>
#include <tuple>

#include <opencv2/opencv.hpp>

#include "preprocessing.hpp"
#include "camera_properties.hpp"
#include "pose.hpp"
#include "features.hpp"

/** @brief Main source file managing salih slam system 
  * Note: this source file acts *less* as real-time way to track positioning, rather simply shows how it's done. The main body of the program (ie. everything besides input initialisation) would be kept the same. Hence, the ROS implementation of this repository acts as a wrapper **/

namespace salih_slam {
    static const std::string& arg_help()
    {
        static const std::string help = "Following arguments are required:\n\t1. YAML file containing camera information (calibration, mapping and pose information)\n\t2. directory of images" ;
        return help ;
    }
}

int main(int argc, char** argv) 
{
    /* Initialisation */
    if(argc != 3)
    {
        std::cerr << "Fatal error: missing arguments" << '\n' ;
        std::cerr << salih_slam::arg_help() << std::endl ;
        return -1  ;
    }

        // extract camera info
    const std::string calibration_file(argv[1]) ;
    const auto camera_info = salih_slam::extract_camera_info(calibration_file) ; // can't do unpacking, want to ensure different mutability access

    const salih_slam::CameraProperties camera_properties(std::get<0>(camera_info)) ; // properties can't change, keep as const
    salih_slam::Pose pose(std::get<1>(camera_info)) ; // is mutable as a concept
    std::cout << camera_properties << '\n' ;
    std::cout << "Initialised " << pose << std::endl ;

        // feature detector object - constant throughout program
    cv::Ptr<cv::ORB> orb_obj = cv::ORB::create() ;

        // note: rest of section strictly is irrelevant to a realtime VSLAM system - you'd substitute this with initialisation of a camera feed etc.
    const std::string directory_name(argv[2]) ;
    if(!salih_slam::is_meta_directory(directory_name))
    {
        std::cerr << "'" << directory_name << "' is not a valid directory or a symlink to a valid directory" << std::endl ; 
        std::cerr << "Fatal error, aborting" << std::endl ;
        return -2 ;
    }
    std::vector<std::string> image_names = salih_slam::get_image_names(directory_name) ;
    std::sort(image_names.begin(), image_names.end()) ; // assume images given are in alphabetical orders of captured frames 

    /* Main body */
    for(auto it = std::cbegin(image_names) ; it != std::prev(std::cend(image_names)) ; it = std::next(it))
    {
        cv::Mat img1(cv::imread(*(it)), cv::Rect(0, 112, 1910, 800)) ; // crop image to get rid of car
        cv::Mat img1_new ; cv::undistort(img1, img1_new, camera_properties.camera_matrix(), camera_properties.distortion_coefficients()) ; 

        const cv::Mat img2(cv::imread(*(it+1)), cv::Rect(0, 112, 1910, 800)) ; // crop image to get rid of car
        cv::Mat img2_new ; cv::undistort(img2, img2_new, camera_properties.camera_matrix(), camera_properties.distortion_coefficients()) ;

        /* Extract features */
        const auto [key_frame_keypoints, key_frame_descriptors] = salih_slam::extract_features(orb_obj, img1_new) ;
        const auto [frame_keypoints, frame_descriptors] = salih_slam::extract_features(orb_obj, img2_new) ;

        /* Get matching features between frames */
        const std::vector<cv::DMatch> matched_features_1 = salih_slam::match_features(key_frame_descriptors, frame_descriptors) ;
        const std::vector<cv::DMatch> matched_features_2 = salih_slam::match_features(frame_descriptors, key_frame_descriptors) ;
        std::vector<cv::DMatch> confirmed_matches = salih_slam::symmetrise_features(matched_features_1, matched_features_2) ; // remove non-symmetric matches
        confirmed_matches = salih_slam::ratio_test_matches(confirmed_matches) ; // perform ratio test to remove matches based on eucilidean distancing differences

            // some visualisation
//        cv::Mat matched_img ; cv::drawMatches(img1_new, key_frame_keypoints, img2_new, frame_keypoints, confirmed_matches, matched_img, cv::Scalar::all(-1), cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS) ;
//        cv::imshow("Feature matched image (using ORB detection algorithm)", matched_img) ;
//        cv::waitKey(0) ;

        /* Calculate pose difference */
        std::vector<cv::Point2f> obj, scene ;
        for(const auto& i : confirmed_matches) // get keypoints only
        {
            obj.push_back(key_frame_keypoints[i.queryIdx].pt) ;
            scene.push_back(frame_keypoints[i.trainIdx].pt) ;
        }
        const cv::Mat obj_mat(obj, true) ; //second param for data copy (here, data are not duplicated!)
        const cv::Mat scene_mat(scene, true) ;

        const cv::Mat essential_matrix = cv::findEssentialMat(obj_mat, scene_mat, camera_properties.camera_matrix()) ;
        if(essential_matrix.empty()) continue ; // essentially if no difference was found and thus matrix returned is empty

        cv::Mat delta_translation, delta_rotation ;
        cv::recoverPose(essential_matrix, obj, scene, camera_properties.camera_matrix(), delta_rotation, delta_translation) ; // amalgamation of decomposeEssentialMat and a cheirality check

        std::cout << "Delta Translation: " << delta_translation << '\n' ;
        std::cout << "Delta Rotation: " << delta_rotation << '\n' ;
        std::cout << std::endl ;
    
        /* Apply pose difference */
        pose.translation(pose.translation() + delta_translation) ;
        std::cout << pose << std::endl ;
    }

    /* E(nd)O(f)P(rogram) */
    return 0 ;
}
